<?php

use App\Models\Answer;
use App\Models\Survey;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('guest')->group(function () {

    Route::get('/login', function () {
        return view('login');
    })->name('login');

    Route::get('/register', function () {
        return view('register', [
            'roles' => DB::table('roles')->get()
        ]);
    })->name('register');

    Route::post('/login', function (Request $request) {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('home');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    });

    Route::post('/register', function (Request $request) {
        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'role' => ['required']
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => intval($request->role),
        ]);

        return redirect()->route('login')->with('success', 'Silahkan login');
    });
});

Route::middleware('auth')->group(function () {

    Route::get('/logout', function (Request $request) {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();


        return redirect()->route('login');
    })->name('logout');

    Route::get('/home', function () {
        return view('home', [
            'data' => auth()->user()->role_id == 1 ?
                Survey::where('user_id', auth()->id())->get() :
                Answer::with(['survey'])->where('user_id', auth()->id())->get()
        ]);
    });

    Route::post('/home', function (Request $request) {
        $survey = Survey::create([
            'uuid' => Uuid::uuid7()->toString(),
            'nama' => $request->nama,
            'point' => $request->point,
            'data' => '{}',
            'attempt' => $request->attempt,
            'user_id' => auth()->id()
        ]);

        return redirect()->route('survey', $survey->uuid);
    })->name('home');

    Route::get('/view/{id}', function ($id) {
        return view('survey', [
            'data' => Survey::where('uuid', $id)->where('user_id', auth()->id())->first()
        ]);
    })->name('survey');

    Route::put('/view/{id}', function ($id, Request $request) {
        return Survey::where('uuid', $id)->where('user_id', auth()->id())->update([
            'data' => $request->data
        ]);
    });

    Route::get('/file', function (Request $request) {
        $path = 'images/' . $request->name;
        return Response::make(Storage::get($path), 200)->header("Content-Type", Storage::mimeType($path));
    });

    Route::post('/file', function (Request $request) {
        /** @var Illuminate\Http\UploadedFile $file */
        $file = collect($request->file())->first();

        $name = Uuid::uuid7()->toString() . '.' . $file->getClientOriginalExtension();
        $file->storeAs('images', $name);

        return response()->json([
            $file->getClientOriginalName() => $name
        ]);
    });

    Route::delete('/file', function (Request $request) {
        return response()->json([
            'status' => Storage::delete('images/' . $request->name)
        ]);
    });

    // analytic
    Route::get('/analytic/{id}', function ($id) {
        return view('analytic', [
            'data' => Survey::with('answers')->where('uuid', $id)->where('user_id', auth()->id())->first()
        ]);
    })->name('analytic');

    // jawab
    Route::get('/{id}', function ($id) {
        if (auth()->user()->role_id == 1) {
            return 'Akun researcher tidak bisa menjawab';
        }

        $Jumlah = Survey::with('answers')->where('uuid', $id)->first()->answers->where('user_id', auth()->id())->count();
        if ($Jumlah != 0) {
            return 'Max 1x isi yaa';
        }

        $survey = Survey::with('answers')->where('uuid', $id)->first();
        $jumlah = $survey->answers->count();

        if ($survey->attempt <= $jumlah) {
            return 'Max ' . $survey->attempt . ' respondent';
        }

        return view('answer', [
            'data' => $survey
        ]);
    })->name('answer');

    Route::post('/{id}', function ($id, Request $request) {
        return Answer::create([
            'uuid' => Uuid::uuid7()->toString(),
            'data' => $request->data,
            'survey_id' => Survey::where('uuid', $id)->first()->id,
            'user_id' => auth()->user()->id
        ]);
    });
});
