<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>My First Survey</title>

    <link href="https://unpkg.com/survey-jquery/defaultV2.min.css" rel="stylesheet" crossorigin="anonymous">
    <style>
        body {
            --primary: #FF7833;
            --secondary: #EF4C29;
        }
    </style>
</head>

<body>
    <div id="surveyContainer"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-jquery/survey.jquery.min.js" crossorigin="anonymous"></script>
    <script>
        (() => {
            const surveyJson = {!! json_encode($data->data) !!};
            const token = "{{ csrf_token() }}";

            const survey = new Survey.Model(surveyJson);

            async function alertResults(sender) {
                const results = JSON.stringify(sender.data);
                await fetch(window.location, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': token
                        },
                        body: JSON.stringify({
                            data: results
                        })
                    })
                    .catch((err) => alert(err));
            }

            survey.onUploadFiles.add(function(survey, options) {

                var formData = new FormData();
                options.files.forEach(function(file) {
                    formData.append(file.name, file);
                });
                var xhr = new XMLHttpRequest();

                xhr.open(
                    "POST",
                    "{{ asset('/') }}file"
                );

                xhr.setRequestHeader('X-CSRF-TOKEN', token);
                xhr.onload = function() {
                    var data = JSON.parse(xhr.responseText);
                    options.callback("success",
                        options.files.map(function(file) {
                            return {
                                file: file,
                                content: data[file.name]
                            };
                        })
                    );
                };

                xhr.send(formData);
            });


            function detectIEOrEdge() {
                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");
                var trident = ua.indexOf("Trident/");
                var edge = ua.indexOf("Edge/");
                return edge > 0 || trident > 0 || msie > 0;
            }

            survey.onDownloadFile.add(function(_, options) {
                var xhr = new XMLHttpRequest();
                xhr.open(
                    "GET",
                    "{{ asset('/') }}file?name=" + options.content
                );

                xhr.onloadstart = function(ev) {
                    xhr.responseType = "blob";
                }
                xhr.onload = function() {
                    var file;
                    if (detectIEOrEdge()) {
                        file = new Blob([xhr.response], options.fileValue.name, {
                            type: options.fileValue.type
                        });
                    } else {
                        file = new File([xhr.response], options.fileValue.name, {
                            type: options.fileValue.type
                        });
                    }
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        options.callback("success", e.target.result);
                    };
                    reader.readAsDataURL(file);
                };
                xhr.send();
            });

            survey.onComplete.add(alertResults);

            $(function() {
                $("#surveyContainer").Survey({
                    model: survey
                });
            });
        })();
    </script>
</body>

</html>
