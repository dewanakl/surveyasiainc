<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h1>Register</h1>
    <form action="/register" method="post">
        @csrf
        <input type="text" name="name" placeholder="nama">
        @error('nama')
            {{ $message }}
        @enderror
        <br>
        <br>
        <input type="text" name="email" placeholder="email">
        @error('email')
            {{ $message }}
        @enderror
        <br>
        <br>
        <input type="text" name="password" placeholder="password">
        @error('email')
            {{ $message }}
        @enderror
        <br>
        <br>
        <label for="role">Role :</label>
        <select id="role" name="role">
            @foreach ($roles as $role)
                <option value="{{ $role->id }}">{{ $role->level }}</option>
            @endforeach
        </select>
        @error('role')
            {{ $message }}
        @enderror
        <br>
        <br>
        <button type="submit">Register</button>
    </form>
    <hr>
    <p>sudah ada akun? <a href="/login">Login</a></p>
</body>

</html>
