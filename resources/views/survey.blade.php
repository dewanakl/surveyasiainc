<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Survey</title>

    <link href="https://unpkg.com/survey-core/defaultV2.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="https://unpkg.com/survey-creator-core/survey-creator-core.min.css" rel="stylesheet"
        crossorigin="anonymous">
    <style>
        body {
            --primary: #FF7833;
            --secondary: #EF4C29;
        }
    </style>
</head>

<body style="margin: 0; padding: 0;">
    <a href="{{ route('home') }}">Kembali</a>
    <div id="surveyCreator" style="height: 100vh;"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/knockout/build/output/knockout-latest.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-core/survey.core.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-knockout-ui/survey-knockout-ui.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-creator-core/survey-creator-core.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-creator-knockout/survey-creator-knockout.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-creator-core/survey-creator-core.i18n.min.js"></script>

    <script>
        (() => {
            const creatorOptions = {
                showLogicTab: true,
                showJSONEditorTab: false,
                haveCommercialLicense: true,
                isAutoSave: true,
                autoSaveDelay: 1000
            };

            const url = new URL(window.location);
            let id = url.pathname.split('/').slice(1)[1].toString();

            const defaultJson = {!! json_encode($data->data) !!};
            const token = "{{ csrf_token() }}";

            SurveyCreator.editorLocalization.currentLocale = 'id';
            const creator = new SurveyCreator.SurveyCreator(creatorOptions);
            creator.toolbox.forceCompact = false;

            function saveSurveyJson(url, json, saveNo, callback) {
                const request = new XMLHttpRequest();
                request.open('PUT', url);
                request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
                request.setRequestHeader('X-CSRF-TOKEN', token);
                request.addEventListener('load', () => {
                    callback(saveNo, true);
                });
                request.addEventListener('error', () => {
                    callback(saveNo, false);
                });
                request.send(JSON.stringify({
                    data: json
                }));
            }

            creator.text = window.localStorage.getItem(id) || defaultJson;
            creator.saveSurveyFunc = (saveNo, callback) => {
                window.localStorage.setItem(id, creator.text);
                saveSurveyJson(
                    window.location,
                    creator.JSON,
                    saveNo,
                    callback
                );
            };

            creator.onCollectionItemDeleting.add(function(_, params) {
                let name = (new URLSearchParams((new URL(params.item.localizableStrings.imageLink
                    .renderedText)).search)).get('name');

                if (!name) {
                    return;
                }

                fetch('{{ asset('/') }}file', {
                    method: "DELETE",
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRF-TOKEN": token
                    },
                    body: JSON.stringify({
                        name: name
                    })
                });
            });

            creator.onUploadFile.add(function(_, options) {
                const formData = new FormData();

                options.files.forEach(function(file) {
                    formData.append(file.name, file);
                });

                fetch('{{ asset('/') }}file', {
                        method: "post",
                        headers: {
                            "X-CSRF-TOKEN": token
                        },
                        body: formData
                    })
                    .then((response) => response.json())
                    .then((result) => {
                        options.callback(
                            "success",
                            "{{ asset('/') }}file?name=" + result[options.files[0].name]
                        );
                    })
                    .catch((error) => {
                        options.callback('error');
                    });
            });

            document.addEventListener("DOMContentLoaded", function() {
                creator.render("surveyCreator");
            });
        })();
    </script>
</body>

</html>
