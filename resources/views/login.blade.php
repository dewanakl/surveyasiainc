<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h1>Login</h1>
    @if ($message = Session::get('success'))
        <strong>{{ $message }}</strong>
    @endif
    <form action="/login" method="post">
        @csrf
        <input type="text" name="email" placeholder="email">
        @error('email')
            {{ $message }}
        @enderror
        <br>
        <br>
        <input type="text" name="password" placeholder="password">
        @error('password')
            {{ $message }}
        @enderror
        <br>
        <br>
        <button type="submit">Login</button>
    </form>
    <hr>
    <p>belum ada akun? <a href="/register">Register</a></p>
</body>

</html>
