<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>Dashboard Survey</title>

    <link crossorigin="anonymous" href="https://unpkg.com/survey-core/defaultV2.min.css" rel="stylesheet" type="text/css">
    <link crossorigin="anonymous" href="https://unpkg.com/survey-analytics/survey.analytics.min.css" rel="stylesheet">
    <style>
        body {
            --primary: #FF7833;
            --secondary: #EF4C29;
        }
    </style>
</head>

<body>
    <a href="{{ route('home') }}">Kembali</a>
    <div id="surveyVizPanel"></div>

    <script src="https://unpkg.com/knockout/build/output/knockout-latest.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-core/survey.core.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-knockout-ui/survey-knockout-ui.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.plot.ly/plotly-latest.min.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/wordcloud/src/wordcloud2.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/survey-analytics/survey.analytics.min.js" crossorigin="anonymous"></script>

    <script>
        (() => {
            /** 
             * ! for example
             */
            const surveyJson = {!! $data->data !!};

            let surveyResults = {!! $data->answers->pluck('data') !!};
            surveyResults.forEach((element, id) => {
                surveyResults[id] = JSON.parse(element);
            });

            const survey = new Survey.Model(surveyJson);
            const vizPanel = new SurveyAnalytics.VisualizationPanel(
                survey.getAllQuestions(),
                surveyResults, {
                    allowHideQuestions: false,
                    haveCommercialLicense: true
                }
            );

            document.addEventListener("DOMContentLoaded", function() {
                vizPanel.render("surveyVizPanel");
            });
        })();
    </script>
</body>

</html>
