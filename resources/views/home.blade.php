<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>

    <a href="{{ route('logout') }}">Logout</a>

    @if (auth()->user()->role_id == 1)
        <h1>Home</h1>

        <form action="{{ route('home') }}" method="post">
            @csrf
            <input type="text" name="nama" placeholder="Nama Survey">
            <br>
            <br>
            <input type="number" name="point" placeholder="Point Survey">
            <br>
            <br>
            <input type="number" name="attempt" placeholder="Jumlah Respondent Survey">
            <br>
            <br>
            <button type="submit">Bikinn</button>
        </form>

        <hr>
        @foreach ($data as $val)
            <h2>{{ $val->nama }}</h2>
            <p>Point :{{ $val->point }}</p>
            <p>Attempt :{{ $val->attempt }}</p>
            <a href="{{ route('survey', $val->uuid) }}">Edit</a>
            <a href="{{ route('analytic', $val->uuid) }}">Analytic</a>
            <button onclick="myFunction('{{ route('answer', $val->uuid) }}')">Copy</button>
        @endforeach
    @else
        <h1>Riwayat</h1>
        <input type="text" id="idsurvey" placeholder="ID survey: xxxx-xxxxx-xxxxx">
        <button onclick="window.location = '{{ asset('/') }}' + document.getElementById('idsurvey').value">Isi
            survey</button>
        @php
            $jumlah = 0;
        @endphp
        @foreach ($data as $val)
            <h2>{{ $val->survey->nama }}</h2>
            <p>Point :{{ $val->survey->point }}</p>
            @php
                $jumlah += $val->survey->point;
            @endphp
            <hr>
        @endforeach
        <h3>Jumlah point :{{ $jumlah }}</h3>
    @endif

    <script>
        function myFunction(copyText) {
            // Select the text field

            // Copy the text inside the text field
            navigator.clipboard.writeText(copyText);

            // Alert the copied text
            alert("Copied the text: " + copyText);
        }
    </script>
</body>

</html>
