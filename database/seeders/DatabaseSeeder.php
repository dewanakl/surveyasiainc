<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        DB::table('roles')->insert([
            'level' => 'researcher'
        ]);

        DB::table('roles')->insert([
            'level' => 'respondent'
        ]);

        DB::table('users')->insert([
            'name' => 'Example respondent',
            'email' => 'respondent@example.com',
            'password' => Hash::make('Abcd!234'),
            'role_id' => 2
        ]);
    }
}
