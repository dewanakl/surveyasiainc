<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Survey;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class SurveyController extends Controller
{
    /**
     * @OA\Get(
     *   path="/api/survey/{id}",
     *   tags={"Survey"},
     *   summary="Survey by ID",
     *   operationId="getSurveyByID",
     * security={
     *         {"bearerAuth": {}}
     *      },
     *@OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *  @OA\Response(
     *      response=401,
     *       description="Unauthorized"
     *   ),
     * @OA\Response(
     *      response=404,
     *       description="Not Found"
     *   ),
     *  @OA\PathItem (
     *  )
     *)
     **/
    public function getSurveyById($id)
    {
        if (auth('api')->user()->role_id == 1) {
            return response()->json([
                'status' => 'error',
                'message' => 'Researcher accounts cannot answer this survey',
            ], 401);
        }

        $survey = Survey::with('answers')->where('uuid', $id)->first();
        if (!$survey) {
            return response()->json([
                'status' => 'error',
                'message' => 'Survey Not Found',
            ], 404);
        }

        $Jumlah = $survey?->answers->where('user_id', auth('api')->id())->count();
        if ($Jumlah > 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'This survey can only be answered once',
            ], 401);
        }

        $survey = $survey->only(['nama', 'data']);
        $survey['data'] = json_decode($survey['data'], false, 1024);

        return response()->json([
            'status' => 'success',
            'survey' => $survey,
        ]);
    }

    /**
     * @OA\Get(
     *   path="/api/survey",
     *   tags={"Survey"},
     *   summary="Survey History",
     *   operationId="getAllsurveyHistory",
     * security={
     *         {"bearerAuth": {}}
     *      },
     *
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *  @OA\Response(
     *      response=401,
     *       description="Unauthorized"
     *   ),
     *  @OA\PathItem (
     *  )
     *)
     **/
    public function getAllHistorySurvey()
    {
        return response()->json([
            'status' => 'success',
            'surveys' => Answer::with('survey')
                ->where('user_id', auth('api')->id())
                ->get()
                ->map(function ($data) {
                    unset($data->survey->data);
                    unset($data->data);
                    return $data;
                })
        ]);
    }

    /**
     * @OA\Post(
     *   path="/api/survey/{id}",
     *   tags={"Survey"},
     *   summary="Simpan survey",
     *   description="Simpan hasil survey dari user",
     *   operationId="savesurvey",
     * security={
     *         {"bearerAuth": {}}
     *      },
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     * @OA\RequestBody(
     *         @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     *   ),
     *     ),
     *   @OA\Response(
     *      response=200,
     *      description="Success",
     *      @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *   ),
     *  @OA\Response(
     *      response=401,
     *       description="Unauthorized"
     *   ),
     * @OA\Response(
     *      response=404,
     *       description="Survey not found"
     *   ),
     *  @OA\PathItem (
     *  )
     *)
     **/
    public function saveSurveyAnswer($id, Request $request)
    {
        $survey = Survey::where('uuid', $id)->first()->id;
        if (!$survey) {
            return response()->json([
                'status' => 'error',
                'message' => 'Survey not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'survey' => Answer::create([
                'uuid' => Uuid::uuid4()->toString(),
                'data' => $request->data,
                'survey_id' => $survey,
                'user_id' => auth('api')->user()->id
            ]),
        ]);
    }
}
